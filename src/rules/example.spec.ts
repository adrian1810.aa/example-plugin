import { HtmlValidate } from "html-validate";
import Plugin from "..";

/* add jest matchers to make testing easier */
import "html-validate/jest";

jest.mock("example-plugin", () => Plugin, { virtual: true });

/* create validator instance and configure it with the plugin */
let htmlvalidate: HtmlValidate;
beforeAll(() => {
	htmlvalidate = new HtmlValidate({
		plugins: ["example-plugin"],
		rules: {
			/* only enable the rule under test */
			"example/rule": "error",
		},
	});
});

it("should report error when <p> is used", () => {
	expect.assertions(2);
	const report = htmlvalidate.validateString("<p></p>");
	expect(report).toBeInvalid();
	expect(report).toHaveError("example/rule", "Paragraphs are not allowed");
});

it("should not report error for other elements", () => {
	expect.assertions(1);
	const report = htmlvalidate.validateString("<u></u>");
	expect(report).toBeValid();
});
