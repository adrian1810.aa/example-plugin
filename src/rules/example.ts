import { Rule } from "html-validate";

export default class ExampleRule extends Rule {
	public setup(): void {
		this.on("dom:ready", ({ document }) => {
			const paragraphs = document.getElementsByTagName("p");
			for (const p of paragraphs) {
				this.report(p, "Paragraphs are not allowed");
			}
		});
	}
}
