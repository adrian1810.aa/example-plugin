import { type Plugin, compatibilityCheck } from "html-validate";
import configs from "./configs";
import rules from "./rules";

interface PackageJson {
	name: string;
	peerDependencies: Record<string, string>;
}

/* eslint-disable-next-line @typescript-eslint/no-var-requires, @typescript-eslint/no-unsafe-assignment */
const pkg: PackageJson = require("../package.json");

/* warn when using unsupported html-validate library version */
if (compatibilityCheck) {
	const range = pkg.peerDependencies["html-validate"];
	compatibilityCheck(pkg.name, range);
}

const plugin: Plugin = {
	name: pkg.name,

	/* Add predefined configurations */
	configs,

	/* Add all rules from rules folder */
	rules,
};

export = plugin;
