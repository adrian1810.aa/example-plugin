import { type ConfigData } from "html-validate";

const config: Record<string, ConfigData> = {
	recommended: {
		rules: {
			"example/rule": "error",
		},
	},
};

export default config;
